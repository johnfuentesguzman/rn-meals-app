import React from 'react';
import {CATEGORIES, MEALS} from '../data/dummy-data'
import MealList from '../components/MealList';

export const CategoriesMealsScreen = ({route,navigation}) => {
    const { selectedCategory } = route.params;
    const displayMeals = MEALS.filter(meal=> meal.categoryId.indexOf(selectedCategory.id) >= 0);
    React.useLayoutEffect(() => {
      navigation.setOptions({
        title: selectedCategory.title
      });
    }, [selectedCategory])

    return (<MealList listData={displayMeals} navigate={navigation} />)
}


  CategoriesMealsScreen.navigationOptions = (navigationData) => {
    console.log(navigationData);
  };

  export default CategoriesMealsScreen;