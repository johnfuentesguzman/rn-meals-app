import React, { useState } from 'react';
import { View, Text, StyleSheet, Switch, Platform } from 'react-native';
import Colors from '../constants/Colors';
import { color } from 'react-native-reanimated';

const FilterSwitch = (props) => {
    return (
        <View style={styles.filterContainer}>
            <Text>{props.filterText}</Text>
            <Switch trackColor={{ true: Colors.filterToogle }}
                    thumbColor={Platform.OS === 'android' ? Colors.primary : Colors.filterToogle} 
                    value={props.valueText}
                    onValueChange={props.onChange} />
        </View>

    )
};

export const FiltersScreen = () => {
    const [isGlutenFree, setIsGlutenFree] = useState(false);
    const [isLactoseFree, setIsLactoseFree] = useState(false);
    const [isVegan, setIsVegan] = useState(false);
    const [isVegetarian, setIsVegetarian] = useState(false);
    return (
        <View  style={styles.screen}>
            <Text style={styles.title}>Filters Availables</Text>
            <FilterSwitch valueText={isGlutenFree} filterText={'Gluten-free'} onChange={(newValue) => setIsGlutenFree(newValue)} />
            <FilterSwitch valueText={isLactoseFree} filterText={'Lactose-free'} onChange={(newValue) => setIsLactoseFree(newValue)} />
            <FilterSwitch valueText={isVegan} filterText={'Vegan'} onChange={(newValue) => setIsVegan(newValue)} />
            <FilterSwitch valueText={isVegetarian} filterText={'Vegetarian'} onChange={(newValue) => setIsVegetarian(newValue)} />
        </View>
    )
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Colors.primary
    },
    filterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '80%',
        marginVertical: 15
    },
    title: {
        fontFamily: 'opens-sans-bold',
        fontSize: 22,
        margin: 20,
        textAlign: 'center'
    }
});
export default FiltersScreen;