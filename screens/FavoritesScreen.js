import React from 'react';
import MealList from '../components/MealList';
import {MEALS} from '../data/dummy-data';

export const FavoritesScreen = (props) => {
  const {navigation} = props;
  const favMeals = MEALS.filter(meal=> meal.id === 'm1');
  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Favorites',
    });
  }, [favMeals])
    return (
        <MealList listData={favMeals} navigate={navigation}/>
    )
}
export default FavoritesScreen;