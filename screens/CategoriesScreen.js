import React from 'react';
import { FlatList, StyleSheet } from 'react-native';
import { CATEGORIES } from '../data/dummy-data';
import CategoryGrid from '../components/CategoryGrid';

const CategoriesScreen = props => {
  const {navigation} = props;
  const renderGridItem = itemData => {
    return <CategoryGrid data={itemData} onSelect={()=> {
      navigation.navigate('CategoriesMealsScreen', {
        selectedCategory: itemData.item,
      });
    }}/>
  };

  return (
    <FlatList
      keyExtractor={(item, index) => item.id}
      data={CATEGORIES}
      renderItem={renderGridItem}
      numColumns={2}
    />
  );

};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default CategoriesScreen;
