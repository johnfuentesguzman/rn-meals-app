import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { MEALS } from '../data/dummy-data';
import CustomHeaderButton from '../components/HeaderButton';
import { HeaderButtons, Item} from 'react-navigation-header-buttons';


export const MealDetailScreen = ({route, navigation}) => {
    const { selectedCategory } = route.params;
    const selectMeal = MEALS.find(meal=> meal.id === selectedCategory);
    React.useLayoutEffect(() => {
        navigation.setOptions({
          title: selectedCategory.title,
          headerRight: () => ( 
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item title='Favorites' iconName='ios-star' onPress={()=> {console.log('favorite button pressed')}}/>
             </HeaderButtons>
          )

        });
      }, [selectedCategory])
  
    return (
        <View style={styles.screen}>
            <Text>{selectMeal.title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({  
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default MealDetailScreen;